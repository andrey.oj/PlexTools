import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import PlexTools from './PlexTools';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<PlexTools />, document.getElementById('root'));

registerServiceWorker();
